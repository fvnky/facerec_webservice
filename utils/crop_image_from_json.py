"""
Given a face bounding_box from the content detection API, generate cropping suggestions
of a face detected in a picture

crop_suggestion(image_path,face_bbox_ratio,saved_image_path)

image_path: path to the image containing the faces
face_bbox: face bounding box from content detection API
saved_image_path: path to save the cropped face to

"""

from PIL import Image
import math
import numpy as np

def new_origin(point,norg):
	"""
	point,norg: (x,y)

	Returns an array of point's coordinates from norg as origin
	"""
	return [point[0]-norg[0],point[1]-norg[1]]

def vec_normalize(vec):
	"""
	vec: terminal point of a vector when initial point is at origin

	Returns vec normalized
	"""
	mag = math.sqrt(vec[0]**2+vec[1]**2)
	return [vec[0]/mag,vec[1]/mag]

def vec_dot(u,v):
	"""
	u,v: (x,y)

	dot product between two vectors
	"""
	return(u[0]*v[0]+u[1]*v[1])

def angle_from_line(line):
	"""
	line: ((x,y),(x,y))

	Returns the angle between line and horizon, in degrees, in range [-180,180]
	"""
	a,b = [],[]
	a.append(line[0][0])
	a.append(line[0][1])
	b.append(line[1][0])
	b.append(line[1][1])
	new_b = new_origin(b,a)
	norm_b = vec_normalize(new_b)
	base = [1,0]
	dotprod = vec_dot(norm_b,base)
	angle = math.degrees(math.acos(dotprod))

	# if the angle is negative
	if (new_b[1]<0):
		angle = -angle
	return angle

def get_rotation_matrix(angle):
	"""
	Returns rotation matrix (as numpy array) for given angle in degrees
	"""

	theta = np.radians(angle)
	c, s = round(np.cos(theta),3), round(np.sin(theta),3)
	rm = np.array(((c,-s),(s,c)))
	return rm

def rotate_point(coord,angle):
	"""
	Rotates point of angle degrees at coord around the origin
	"""

	rm = get_rotation_matrix(angle)
	r_point = np.dot(np.array(coord),rm)
	r_point_t = tuple([round(x,3) for x in r_point.tolist()])
	return r_point_t

def rotate_point_offset_center(coord, angle, center):
	"""
	Rotates point of angle degrees at coord around the given center (as x,y)
	"""
	rc_offset = rotate_point((coord[0]-center[0],coord[1]-center[1]),angle)
	return [round(rc_offset[0]+center[0],3),round(rc_offset[1]+center[1],3)]

def rotate_bounding_box(bbox, angle, center=(0,0)):
	"""
	bbox: dict (key, coords (x,y))
	Returns a copy of bbox rotated <angle> degrees around the center
	"""
	rbbox = {}
	for x in bbox:
		rbbox[x] = rotate_point_offset_center(bbox[x],angle,center)
	return rbbox

def bbox_ratio_to_pxl(bbox_ratio,image_size):
	"""
	bbox_ratio: dict (key, coords as (x,y))
	image_size: (width,height)

	Returns a copy of bbox_ratio where distances are expressed in pixels
	"""
	bbox_pxl = {}
	for x in bbox_ratio:
		bbox_pxl[x] = (round(bbox_ratio[x][0]*image_size[0]),round(bbox_ratio[x][1]*image_size[1]))
	return bbox_pxl

def coords_in_new_canvas(coords,org_size,new_size):
	"""
	coords: tuple (x,y)
	org_size: tuple (x,y)
	new_size: tuple (x,y)
	"""

	x_padding = (new_size[0]-org_size[0])/2
	y_padding = (new_size[1]-org_size[1])/2
	return (coords[0]+x_padding,coords[1]+y_padding)

def zero_or_above(number):
	return 0 if number < 0 else number

def crop_face(bbox,image,border_coeff=0.6):
	"""
	bbox: {"nw":(x,y),"ne":(x,y),"se":(x,y),"sw":(x,y)}
	image
	Returns PIL image cropped around given bbox with a padding
	"""

	bbox_values = list(bbox.values())

	top = int(min(list(coord[1] for coord in list(bbox.values()))))
	left = int(min(list(coord[0] for coord in list(bbox.values()))))
	bottom = int(max(list(coord[1] for coord in list(bbox.values()))))
	right = int(max(list(coord[0] for coord in list(bbox.values()))))
	width = right-left
	height = bottom-top

	top = zero_or_above(int(top - height * border_coeff))
	right = zero_or_above(int(right + width * border_coeff))
	bottom = zero_or_above(int(bottom + height * border_coeff))
	left = zero_or_above(int(left - width * border_coeff))

	face_image = np.array(image)[top:bottom,left:right]
	crop_image = Image.fromarray(face_image)
	return crop_image

def crop_suggestion(image,face_bbox_ratio,saved_image_path):
	"""
	image: PIL image to crop
	face_bbox_ratio: oriented bounding box containing the face to crop
	"""

	# Convert bounding box coordinates to have distances in pixels
	bbox_face_coordinates = bbox_ratio_to_pxl(face_bbox_ratio,image.size)
	
	# Get angle of the face
	upper_edge = (tuple(bbox_face_coordinates["nw"]),tuple(bbox_face_coordinates["ne"]))
	angle = angle_from_line(upper_edge)
	if (abs(angle)<15):
		angle = 0

	# Rotate (and extend) the whole image to adjust to face orientation
	rotated_image = image.rotate(angle, resample=Image.BICUBIC,expand=1)
	rotated_image_center = (rotated_image.size[0]/2,rotated_image.size[1]/2)
	bbox_new_size = {}
	for c in bbox_face_coordinates:
		bbox_new_size[c] = coords_in_new_canvas(tuple(bbox_face_coordinates[c]),image.size,rotated_image.size)
	# rotate bbox around image center in new extended image
	rbbox = rotate_bounding_box(bbox_new_size,angle,rotated_image_center)
	cropped = crop_face(rbbox, rotated_image,0.6)
	cropped.save(saved_image_path,'jpeg')