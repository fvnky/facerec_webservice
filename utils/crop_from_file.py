import sys
from crop_image_from_json import crop_suggestion
import json
import os
from PIL import Image

CROPPED_IMAGE_SUFFIX = "_cropped"

if __name__ == "__main__":
	args = sys.argv[1:]
	if len(args) != 2: 
		sys.stderr.write("Usage: {} <image_to_crop> <jsonfile.json>")
		exit(1)

	jsonfile = sys.argv[2]
	imagepath = sys.argv[1]
	savedpath = list(os.path.splitext(imagepath))
	savedpath = savedpath[0] + CROPPED_IMAGE_SUFFIX + savedpath[1]

	try:
		image = Image.open(imagepath)
	except e:
		sys.stderr.write("Error while trying to open {}: {}".format(imagepath,e))
		exti(1)

	if os.path.splitext(jsonfile)[1] != ".json":
		sys.stderr.write("Please submit a file with a .json extension")
		sys.stderr.write("Usage: {} <image_to_crop> <jsonfile.json>")
		exit(1)

	api_response = open(jsonfile).read()
	data = json.loads(api_response)

	if not "face" in data["response"]["content_types"].split(","):
		print("Response does not include any face information.")
		exit(0)
	elif data["response"]["face_detection"]["faces_in_image"] == 0:
		print("No face found in image.")
		exit(0)

	for face in data["response"]["face_detection"]["face_locations"]:
		face_bbox = face["bounding_box"]
		crop_suggestion(image,face_bbox,savedpath)
