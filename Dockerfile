FROM python:3.6.5-slim-stretch

# TF deps
RUN apt-get update && apt-get install -y --no-install-recommends \
        build-essential \
        curl \
        libfreetype6-dev \
        libhdf5-serial-dev \
        libpng-dev \
        libzmq3-dev \
        pkg-config \
        python3-dev \
        rsync \
        software-properties-common \
        unzip \
        && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

RUN curl -O https://bootstrap.pypa.io/get-pip.py && \
    python get-pip.py && \
    rm get-pip.py

RUN pip --no-cache-dir install \
        Pillow \
        h5py \
        ipykernel \
        #matplotlib \
        numpy \
        pandas \
        scipy \
        sklearn \
        && \
    python -m ipykernel.kernelspec

RUN  pip3 --no-cache-dir install https://files.pythonhosted.org/packages/22/c6/d08f7c549330c2acc1b18b5c1f0f8d9d2af92f54d56861f331f372731671/tensorflow-1.8.0-cp36-cp36m-manylinux1_x86_64.whl

RUN apt-get update && apt-get install -y --fix-missing --no-install-recommends \
    cmake \
    gfortran \
    git \
    wget \
    graphicsmagick \
    libgraphicsmagick1-dev \
    libatlas-dev \
    libavcodec-dev \
    libavformat-dev \
    libgtk2.0-dev \
    libjpeg-dev \
    liblapack-dev \
    libswscale-dev \
    python3-numpy \
    zip \
    && apt-get clean && rm -rf /tmp/* /var/tmp/*

RUN cd ~ && \
    mkdir -p dlib && \
    git clone -b 'v19.9' --single-branch https://github.com/davisking/dlib.git dlib/ && \
    cd  dlib/ && \
    python3 setup.py install --yes USE_AVX_INSTRUCTIONS && \
    cd .. && rm -rf dlib/

RUN pip3 install --no-cache-dir uwsgi

RUN apt-get update && apt-get install -y --no-install-recommends \
    nginx \
    && apt-get clean && rm -rf /tmp/* /var/tmp/*

COPY ws_nginx_conf /etc/nginx/sites-available/

# write nginx logs to /webservice/data/logs/
RUN sed -i "s/\/var\/log\/nginx\//\/webservice\/data\/logs\/nginx_/g" /etc/nginx/nginx.conf

COPY requirements.txt /requirements.txt

RUN pip install -r /requirements.txt && \
    rm /requirements.txt

COPY webservice /webservice

RUN ln -s /etc/nginx/sites-available/ws_nginx_conf /etc/nginx/sites-enabled/ws_nginx_conf && \
    useradd -M web_service -g www-data && \
    export PYTHONPATH=/webservice/object_detection && \
    chown web_service -R /webservice

ENV TZ=Europe/Paris
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

WORKDIR /webservice
EXPOSE 8000/tcp
ENTRYPOINT ["/webservice/start.sh"]
