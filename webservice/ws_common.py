import os

USER_AGENT = "Content Detection"
TF_MODELS_FOLDER = os.path.join("data","content_detection_models")
ALLOWED_EXTENSIONS = {'png', 'jpg', 'jpeg', 'gif'}

DEFAULT_FORM = '''
    <!doctype html>
    <title>Content detection API</title>
    <h1>Please submit a valid image</h1>
    through uri params:'?url=http://url.to/your/image'
    or using the form below
    <form method="POST" enctype="multipart/form-data">
    <input type="file" name="file">
    <input type="submit" value="Upload">
    </form>
    '''