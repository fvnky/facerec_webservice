import flask
import urllib.request
import numpy as np
import six.moves.urllib as urllib
import time
import os
import glob
import random
from facerec.facerec_common import get_facerec_tables
from PIL import Image
from object_detection.utils import ops as utils_ops
from ws_facerec import detect_faces_in_image
from ws_content_detection import inference_for_all_types
from object_detection.utils import label_map_util
from object_detection.utils import visualization_utils as vis_util
from urllib.request import urlopen, Request
from ws_common import *

app = flask.Flask(__name__)

def status(status,infos=""):
    return ({"status":status,
            "info":infos})

def load_image_into_numpy_array(image):
    (im_width, im_height) = image.size
    return np.array(image.convert("RGB").getdata()).reshape(
        (im_height, im_width, 3)).astype(np.uint8)

def resize_if_needed(image, width):
    """
    image: PIL image

    Returns a PIL image rescaled to have given width if it's wider
    returned image has same aspect ratio
    """
    resized = image
    if (image.size[0] > width):
        scale = width/image.size[0]
        resized = image.resize((int(image.size[0]*scale),int(image.size[1]*scale)),resample=Image.BICUBIC)
    return resized

def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

def get_models_list():
    model_list=[]
    model_files = glob.glob(TF_MODELS_FOLDER+"/*")
    for model in model_files:
        model_ext = os.path.splitext(model)[-1]
        if(os.path.isfile(model) and model_ext == ".pb"):
            model_name=os.path.basename(model)[:-3]
            model_list.append(model_name)
    return model_list

def checkURL(url):
    try:
        req = Request(
            url, 
            data=None, 
            headers={
                'User-Agent': USER_AGENT
            }
        )
        urlopen(req)
    except:
        return False
    else:
        return True

def detect_content(image,content_types,facetables):
    tmp1 = int(time.time())
    image_np = load_image_into_numpy_array(image)
    response = {}
    items_dict = {}
    item_count = 0

    response["content_types"] = (", ").join(content_types)
    if "face" in content_types:
        face_detection = detect_faces_in_image(image,facetables)
        response["face_detection"] = face_detection
        content_types.remove("face")

    for item_class in content_types:
        try:
            inference_for_all_types(image_np,item_class,items_dict)
        except Exception as err:
            print("Exception: {}".format(err))

    item_count = len(items_dict)
    if item_count:
        response["item_detection"] = items_dict

    response["items_count"] = item_count
    response["timestamp"] = tmp1
    return flask.jsonify({"status":"success","response":response})

def handle_url(url,content_types,facetables):
    if (checkURL(url)):
        opener = urllib.request.build_opener()
        opener.addheaders = [('User-agent', USER_AGENT)]
        urllib.request.install_opener(opener)
        filename="Image"+str(random.random())[2:]+".jpg"
        urllib.request.urlretrieve(url, filename)
        try:
            image = resize_if_needed(Image.open(filename),600)
        except:
            return flask.jsonify(status("failure","not a valid image file"))
            os.remove(filename)
        os.remove(filename)
        return detect_content(image.convert('RGB'),content_types,facetables)
    else:
        return flask.jsonify(status("failure","not a valid url"))

def handle_file(file,content_types,facetables):
    if file.filename == '':
        return flask.redirect(flask.request.url)
    if file and allowed_file(file.filename):
        try:
            img = resize_if_needed(Image.open(file),600)
        except:
            return flask.jsonify(status("failure","not a valid image file"))
        return detect_content(img.convert('RGB'),content_types,facetables)
    else:
        return flask.jsonify(status("failure","not a valid image file"))

@app.route('/', methods=['GET', 'POST'])
def upload_image():

    valid_content_type = get_models_list()
    valid_content_type.append("face")
    valid_query_models = []
    facetables = []
    cdstr = flask.request.args.get("content_detection")
    recstr = flask.request.args.get("face_recognition")
    url = flask.request.args.get('url')

    if cdstr:
        query_models = set(cdstr.split(","))
        valid_query_models = list(query_models.intersection(set(valid_content_type)))
    
    if recstr:
        splitrecstr = recstr.split(",")
        facetables = list(filter(lambda t: t.split(":")[0] in get_facerec_tables(), splitrecstr))
        if facetables and not "face" in valid_query_models:
            valid_query_models.append("face")

    if url:
        return handle_url(url,valid_query_models,facetables)
    elif 'file' in flask.request.files:
        file = flask.request.files['file']
        return handle_file(file,valid_query_models,facetables)
    else:
        return DEFAULT_FORM

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=5001, debug=False)