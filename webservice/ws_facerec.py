import face_recognition
from PIL import Image
import time
import numpy as np
import math
from facerec.facerec_query_db import face_lookup

def face_locations_to_dict(face_location):
    """
    face_location: list of 4 tuples (x,y)

    Return a dict of bbox corners (as north-west,north-east etc.) from face_location
    """
    return {"nw":face_location[0], 
            "ne":face_location[1], 
            "sw":face_location[2],
            "se":face_location[3]}

def get_bbox_center(bbox_corners):
    """
    bbox_corners: tuple of 4 points (x,y)

    Return coordinates to the center of the bbox
    """
    x = list(c[0] for c in bbox_corners)
    y = list(c[1] for c in bbox_corners)
    max_x = max(x)
    max_y = max(y)
    min_x = min(x)
    min_y = min(y)
    bb_width = max_x - min_x
    bb_height = max_y - min_y
    bb_center_x = min_x + (bb_width/2)
    bb_center_y = min_y + (bb_height/2)
    return (bb_center_x,bb_center_y)

def rotate_and_get_faces(image):
    """
    image: PIL image

    Analyzes a PIL image to locate faces
    If needed, rotates it by increment of 30 degrees until a face is found
    Returns a list of faces (as bounding boxes), the angle of rotation and
    the rotated image
    """
    i = 1
    num_faces = 0
    while (i <= 12) and (num_faces == 0):
        start = time.time()
        # get angles 0,30,-30,60,-60 etc.
        angle = (i*15) if (i%2 == 0) else ((i-1)*(-15))
        rotated = image.rotate(angle, expand=1)
        image_np = np.array(rotated)
        fl_list = face_recognition.face_locations(image_np)
        num_faces = len(fl_list)
        stamp = time.time() - start
        i += 1
    return fl_list,angle,rotated

def get_eyeline_angle(face_landmarks):
    """
    face_landmarks: list of face_landmarks

    Returns the angle of the eyes line found in face_landmarks
    """
    eyes_line = [face_landmarks["left_eye"][0],face_landmarks["right_eye"][-3]]
    eyes_line_angle = 0
    if (eyes_line[1][1]-eyes_line[0][1]) != 0:
            eyes_line_slope = (eyes_line[0][1]-eyes_line[1][1])/(eyes_line[0][0]-eyes_line[1][0])
            eyes_line_angle = math.degrees(math.atan(eyes_line_slope))
    return eyes_line_angle

def get_bbox_corners(bbox_css):
    """
    bbox_css: bounding box boundaries as css relative float values (top right bottom left)
    im_size: dimensions of the image in pixels (int)

    Returns a cw-ordered tuple of corners (as (x,y) coordinates in pixels from origin)
    of the bounding box
    Starting with top left corner (north west)
    """
    top, right ,bottom, left = (bbox_css)
    nw = tuple(round(c) for c in (left,top))
    ne = tuple(round(c) for c in (right,top))
    sw = tuple(round(c) for c in (left,bottom))
    se = tuple(round(c) for c in (right,bottom))
    return (nw,ne,se,sw)

def get_rotation_matrix(angle):
    """
    angle: rotation angle in degrees
    Returns rotation matrix (as numpy array) for given angle in degrees
    """
    theta = np.radians(angle)
    c, s = round(np.cos(theta),3), round(np.sin(theta),3)
    rm = np.array(((c,-s),(s,c)))
    return rm

def rotate_coords(coord,angle):
    """
    coord: (tuple of coordinates x,y in pixels from origin)
    angle: rotation angle in degrees

    Rotates point at <coord> 
    of <angle> degrees around the origin
    """
    rm = get_rotation_matrix(angle)
    r_point = np.dot(np.array(coord),rm)
    r_point_t = tuple([round(x,3) for x in r_point.tolist()])
    return r_point_t

def rotate_coords_offset_center(coord, angle, center):
    """
    coord: (tuple of coordinates x,y in pixels from origin)
    angle: rotation angle in degrees
    center: (tuple of coordinates x,y in pixels from origin)

    Rotates point at <coord> (tuple of coordinates x,y in pixels from origin)
    of <angle> degrees around the given center
    """
    rc_offset = rotate_coords((coord[0]-center[0],coord[1]-center[1]),angle)
    return round(rc_offset[0]+center[0],3),round(rc_offset[1]+center[1],3)

def keep_distance_from_center(coords,size1,size2):
    """
    coords: (tuple of coordinates x,y in pixels from origin)
    size1: tuple of dimensions of the original image
    size2: tuple of expanded dimensions of the image

    Returns the position 
    (e.g after a rotate)
    """
    center2 = (size2[0]/2,size2[1]/2)
    center1 = (size1[0]/2,size1[1]/2)
    x_from_center1 = coords[0] - center1[0]
    y_from_center1 = coords[1] - center1[1]
    return (x_from_center1+center2[0],y_from_center1+center2[1])

def detect_faces_in_image(pil_image,tables,tolerance=0.5):

    processing_start = time.time()
    img = pil_image.convert('RGB')
    image_width = img.size[0]
    image_height = img.size[1]
    recognized = None
    result = {}

    # rotate image in 30 degree increments until at least one face is found
    # image boundaries are expanded if rotated
    face_locations, image_angle, face_image = rotate_and_get_faces(img)

    num_faces = len(face_locations)

    faces = []

    for i in range(num_faces):
        face_bbox = face_locations[i]
        (nw,ne,se,sw) = list(get_bbox_corners(face_bbox))
        r_img = img.rotate(image_angle, resample=Image.BICUBIC,expand=1)
        expanded_canvas_center = r_img.size[0]/2,r_img.size[1]/2
        # getting face landmarks from image rotated at the angle at which the face was found
        face_landmarks_list = face_recognition.face_landmarks(np.array(r_img), face_locations)
        face_angle = get_eyeline_angle(face_landmarks_list[i])
        bb_center = get_bbox_center((nw,ne,se,sw))
        # rotate the bounding box around its center to align with the eyes_line
        (nw,ne,se,sw) = tuple([rotate_coords_offset_center(point,-face_angle,bb_center) for point in (nw,ne,se,sw)])   
        # rotate the bounding box around the rotated image center
        (nw,ne,se,sw) = tuple([rotate_coords_offset_center(point,-image_angle,expanded_canvas_center) for point in (nw,ne,se,sw)])
        # project bounding box coordinates on the original image's dimensions
        (nw,ne,se,sw) = tuple(keep_distance_from_center(p,r_img.size,img.size) for p in (nw,ne,se,sw))

        # normalizing coordinates
        bb_corners_rel = [  (nw[0]/image_width,nw[1]/image_height),
                            (ne[0]/image_width,ne[1]/image_height),
                            (se[0]/image_width,se[1]/image_height),
                            (sw[0]/image_width,sw[1]/image_height) ]

        rotated_face_locations = [(round(i,3),round(j,3)) for (i,j) in bb_corners_rel]
        
        facedict = face_locations_to_dict(rotated_face_locations)

        # Return the result as dict
        face = {"face_id":i,"bounding_box":facedict, "face_angle":round(face_angle,3)}

        # Face recognition
        if tables:
            face["recognized"] = "none"
            for t in tables:
                tsplit = t.split(':')
                table_id = tsplit[0]
                if len(tsplit) > 1:
                    try:
                        table_tol = max(0, min(float(tsplit[1]),1))
                    except:
                        table_tol = float(tolerance)
                else:
                    table_tol = float(tolerance)
                face_enc = face_recognition.face_encodings(np.array(face_image),[face_bbox])
                recognized = face_lookup(table_id,face_enc,table_tol)
                face["tolerance"] = table_tol
                if recognized:
                    face["recognized"] = recognized
                    break
        faces.append(face)

    result["face_locations"] = faces
    result["image_angle"]=image_angle
    result["faces_count"] = num_faces
    return result