import os

FACES_FILE_TYPES = [".jpeg",".jpg",".png",".bmp"]
FACEREC_TABLES = ["scammers","celebrities","stolen_profiles"]
WEBSERVICE_ROOT=os.sep+"webservice"
WEBSERVICE_DATA=os.path.join(WEBSERVICE_ROOT,"data")
FACEREC_DATA = os.path.join(WEBSERVICE_ROOT,"data","facerec")
FACEREC_TABLES_LIST = os.path.join(FACEREC_DATA,"tables_list.txt")
WATCHDOG_LOG_NAME = "facerec_db_watchdog.log"
WATCHDOG_LOG_SIZE = 1024**2*3 #Bytes
LOG_PATH = os.path.join(WEBSERVICE_DATA,"logs")
DB_NAME = 'encodings.sqlite'
DB_PATH = os.path.join(FACEREC_DATA,DB_NAME)

DB_INSERT_TEMPLATE = "INSERT into {}(name, encodings) values (?,?)"
DB_CREATE_TEMPLATE = "CREATE TABLE IF NOT EXISTS {} (name TEXT UNIQUE, encodings BLOB)"
DB_DELETE_TEMPLATE = "DELETE from {} where name like ?"

def get_facerec_tables():
	tables = []
	try:
		tablefile = open(FACEREC_TABLES_LIST,"r")
		tables = [line.rstrip('\n').strip() for line in tablefile]
	except Exception:
		print("Error while trying to open {}".format(FACEREC_TABLES_LIST))
	return tables