"""
Saves the face encodings of each face in a SQLite database with their filename
Face encodings are numpy arrays of 128 64-bit floating-point values stored in little endian
(dtype = "<f8")
"""

import face_recognition
import sys
import os
import io
import zipfile
import glob
import sqlite3
import numpy as np
import argparse
import logging
from PIL import Image
from facerec_common import *

def rotate_and_process(image,process):
	"""
    image: PIL image
    process: function taking PIL image as parameter

    Analyzes a PIL image to locate faces
    If needed, rotates it by increment of 30 degrees until process returns
    Returns results from process as a list and the angle of rotation
    """
	i = 1
	results = []
	while (i <= 12) and (len(results) == 0):
		angle = (i*15) if (i%2 == 0) else ((i-1)*(-15))
		# get angles 0,30,-30,60,-60 etc.
		rotated = image.rotate(angle, expand=1)
		# convert PIL image into numpy array for face recognition
		image_np = np.array(rotated)
		results = process(image_np)
		i += 1
	return results,angle

def resize_if_needed(image, max_width=600, min_width=128, rfilter=Image.LANCZOS):
    """
    image: PIL image

    Returns a PIL image rescaled to have a width within given range if necessary
    """
    resized = image
    scale = 1
    if (image.size[0] > max_width):
        scale = max_width/image.size[0]
    elif (image.size[0] < min_width):
    	scale = min_width/image.size[0]
    resized = image.resize((int(image.size[0]*scale),int(image.size[1]*scale)),resample=rfilter)
    return resized
    
def add_from_dir(faces_dir,db_conn,db_insert,logger):
	"""
	faces_dir: path to a directory
	db_conn: connection to a sqlite3 database
	db_insert: parametrized insert sql statement
	logger: logger to write to

	Adds all the face encodings extracted from recognized filetypes in 
	faces_dir to a sqlite3 database
	"""
	logger.info("Adding faces from directory {}".format(faces_dir))
	files = []
	for file in glob.glob(faces_dir+"/*"):
		if os.path.isdir(file):
			add_from_dir(file)

	for ft in FACES_FILE_TYPES:
		files.extend(glob.glob(faces_dir+"/*"+ft))
	for f in files:
		add_from_file(f,db_conn,db_insert)
	db_conn.close()
	return 0

def add_from_archive(zipped_pictures,db_conn,db_insert,logger):
	"""
	zipped_pictures: path to a zip archive containing images
	db_conn: connection to a sqlite3 database
	db_insert: parametrized insert sql statement
	logger: logger to write to

	Adds all the face encodings extracted from recognized filetypes in 
	zipped_pictures to a sqlite3 database
	"""
	try:
		with zipfile.ZipFile(zipped_pictures) as encodings_archive:
			for name in encodings_archive.namelist():
				if os.path.splitext(name)[-1] not in FACES_FILE_TYPES:
					logger.info("{} does not have an image file extension!".format(name))
					continue
				logger.info("Adding {} to database.".format(name))
				imagedata = encodings_archive.read(name)
				imagebytes = io.BytesIO(imagedata)
				try:
					img = Image.open(imagebytes)
				except IOError as ioe:
					logger.error("Could not open '{}' as image.".format(name))
					continue
				if len(img.getbands()) > 3:
					img = img.convert("RGB")
				image = resize_if_needed(img)
				np_encodings,angle = rotate_and_process(img,face_recognition.face_encodings) #we only use np_encodings
				if (len(np_encodings) > 0):
					encodings = np_encodings[0]
					try:
						recordname = os.path.join(zipped_pictures,name)
						with db_conn:
							db_conn.execute(db_insert,(recordname,encodings))
					except sqlite3.IntegrityError:
						logger.warning("Encodings for file '{}' already in database, skipping...".format(name))
				else:
					logger.info("Could not extract any face feature from '{}'".format(name))
	except zipfile.BadZipFile as bzf:
		logger.error("Could not open '{}' as zip file".format(zipped_pictures))
		return 1
	return 0

def add_from_file(face_image,db_conn,db_insert,logger):
	"""
	face_image: path to an image
	db_conn: connection to a sqlite3 database
	db_insert: parametrized insert sql statement
	logger: logger to write to

	Adds all the face encodings extracted from face_image to a sqlite3 database
	"""
	logger.info("Adding {} to database.".format(face_image))
	try:
		img = Image.open(FACEREC_DATA+os.sep+face_image)
	except IOError as ioe:
		logger.error("Could not open '{}' as image.".format(face_image))
		return 1
	if len(img.getbands()) > 3:
		img = img.convert("RGB")
	img = resize_if_needed(img)
	np_encodings,angle = rotate_and_process(img,face_recognition.face_encodings)
	if (len(np_encodings) > 0):
		encodings = np_encodings[0]
		name = (face_image)
		try:
			with db_conn:
				db_conn.execute(db_insert,(name,encodings))
		except sqlite3.IntegrityError:
			logger.warning("Encodings for file '{}' already in database, skipping...".format(name))
	else:
		logger.info("Could not extract any face feature from '{}'".format(face_image))
	return 0

def delete_like(db_conn,db_delete,name,logger):
	"""
	db_conn: connection to a sqlite3 database
	db_delete: parametrized delete sql statement
	name: string, name of the record to delete
	logger: logger to write to
	
	Adds all the face encodings extracted from face_image to a sqlite3 database
	"""
	logger.info("Deleting like {}".format(name))
	try:
		with db_conn:
			db_conn.execute(db_delete,(name+'%',))
	except sqlite3.DatabaseError as e:
		logger.exception("Exception while deleting like {}: {}".format(name,e))

if __name__ == "__main__":
	ap = argparse.ArgumentParser()
	ap.add_argument("-d","--faces_dir",required=False, type=str,
		help="path to a directory containing faces to add to the recognition database")
	ap.add_argument("-a","--faces_archive",required=False, type=str,
		help="path to a .zip archive containing faces to add to the recognition database")
	ap.add_argument("-f","--face_file",required=False, type=str,
		help="path to a picture containing faces to add to the recognition database")
	ap.add_argument("-db","--database",required=True, type=str,
		help="path to the database containing the encodings of the faces to recognize")
	ap.add_argument("-t","--table",required=True, type=str,
		help="name of the table to add the encodings to. Available tables: {}".format(available_tables))
	args = vars(ap.parse_args())

	db_path = args['database']
	table = args['table']
	if table not in get_facerec_tables():
		print("[ERROR] '{}' is not a valid table name.\nPlease pick one within {}"\
			.format(table,get_facerec_tables()))
		exit(1)

	db_insert = DB_INSERT_TEMPLATE.format(table)
	db_delete = DB_DELETE_TEMPLATE.format(table)

	try:
		db_conn = sqlite3.connect(db_path)
	except:
		print("[ERROR] Could not connect to the database file '{}'".format(db_path))
		exit(1)

	db_conn.execute(DB_CREATE_TEMPLATE.format(table))
	db_conn.execute('PRAGMA journal_mode=wal')
	
	if args['faces_dir'] != None:
		if not os.path.isdir(args['faces_dir']):
			print("[ERROR] '{}' is not a directory".format(args['faces_dir']))
			db_conn.close()
			exit(1)
		else:
			add_from_dir(args['faces_dir'],db_conn,db_insert)

	if args['face_file'] != None:
		if not os.path.isfile(args['face_file']):
			print("[ERROR] '{}' is not a regular file".format(args['face_file']))
			db_conn.close()
			exit(1)
		else:
			add_from_file(args['face_file'],db_conn,db_insert)
	
	if args['faces_archive'] != None:
		if not os.path.isfile(args['faces_archive']):
			print("[ERROR] '{}' is not a regular file".format(args['faces_archive']))
			db_conn.close()
			exit(1)
		else:
			add_from_archive(args['faces_archive'],db_conn,db_insert)

	db_conn.close()
	exit(0)