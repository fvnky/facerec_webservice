"""
Parameters:
db
table
face_image
tolerance
"""

import argparse
import sqlite3
import sys
import os
import glob
import random
import numpy as np
import face_recognition
from .facerec_common import *
from PIL import Image

global db_conn

def face_lookup(table,unknown_encodings,rec_tol=0.3275):
	"""
	table: name of the table containing faces to lookup
	unknown_encodings: 	encodings of the face to recognize extracted with face_recognition api
						(np array of 128 64bit floating points values)
	rec_tol: tolerance for face recognition
	
	If the face encoded in unknown_encodings is found in table, returns the match name in table
	"""
	try:
		db_conn = sqlite3.connect(DB_PATH)
	except:
		print("[ERROR] Could not connect to the database file '{}'".format(DB_PATH))
		return None
	try:
		with db_conn:
			for row in db_conn.execute("SELECT * FROM {}".format(table)):
				database_encodings = np.fromstring(row[1],dtype='<f8')
				match = face_recognition.compare_faces([database_encodings], unknown_encodings[0], tolerance=rec_tol)
				if match[0]:
					return row[0]
	except sqlite3.DatabaseError as de:
		return None
		print("[ERROR] While trying to access '{}': {}".format(table,de))
		return None
	return None

if __name__ == "__main__":
	ap = argparse.ArgumentParser()
	ap.add_argument("-f","--face_file",required=True, type=str,
		help="path to a picture containing unknown faces")
	ap.add_argument("-db","--database",required=True, type=str,
		help="path to the database containing the encodings of the faces to recognize")
	ap.add_argument("-tb","--table",required=True, type=str,
		help="name of the table containing the encodings of the people we want to recognize.")
	ap.add_argument("-tol","--tolerance",required=False, type=str,
		help="tolerance of the recognition (must be < 1; the lower value, the stricter the recognition)")
	args = vars(ap.parse_args())

	db_path = args['database']
	table = args['table']
	tolerance = args['tolerance']
	available_tables = get_facerec_tables()
	if table not in available_tables:
		print("[ERROR] The specified table name '{}' is not present in the database.\nPlease pick one within {}"\
			.format(table,available_tables))
		exit(1)

	if args['face_file'] != None:
		if not os.path.isfile(args['face_file']):
			print("[ERROR] No file named '{}'".format(args['face_file']))
			exit(1)

	if db_path != None:
		if not os.path.isfile(db_path):
			print("[ERROR] No database found with name '{}'".format(db_path))
			exit(1)

	if tolerance:
		try:
			tolerance = float(tolerance)
		except:
			print("[ERROR] Tolerance '{}' is not a number".format(tolerance))
			exit(1)
		if tolerance > 1:
			print("[ERROR] Tolerance '{}' is greater than 1".format(tolerance))
			exit(1)

	unknown_person = args["face_file"]
	img = face_recognition.load_image_file(unknown_person)
	unknown_face_enc = face_recognition.face_encodings(img)
	face_found = False
	if unknown_face_enc:
		if tolerance:
			face_found = face_lookup(table,unknown_face_enc,tolerance)
		else:
			face_found = face_lookup(table,unknown_face_enc)

	if face_found:
		print (":) face found!\nlooks like {}".format(face_found))
	else:
		print(":( no face recognized!")