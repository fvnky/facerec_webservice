#!/usr/local/bin/python
import os
import sys
import time
import glob
import sqlite3
import logging
import logging.handlers
from watchdog.observers.polling import PollingObserver
from watchdog.events import FileSystemEventHandler
from facerec_common import *
from facerec_manage_db import add_from_file,add_from_dir,add_from_archive,delete_like

class FacerecFilesHandler(FileSystemEventHandler):

	def on_created(self, event):

		logger = logging.getLogger('facerec_db_watchdog')
		if event.is_directory:
			return

		path = event.src_path
		logger.debug("Event: file created: {}".format(path))
		self.event_add_to_db(path)
		return

	def on_moved(self, event):
		
		logger = logging.getLogger('facerec_db_watchdog')
		if event.is_directory:
			return

		path = event.src_path
		logger.debug("Event: file moved: {}".format(path))
		self.event_add_to_db(path)
		return

	def event_add_to_db(self,path):
		data_file_path = path.replace(FACEREC_DATA+os.sep,"")
		logger.debug("filepath: {}".format(data_file_path))
		basedir = data_file_path.split(os.sep)[0]
		if basedir not in get_facerec_tables():
			return

		db_insert = DB_INSERT_TEMPLATE.format(basedir)
		db_create = DB_CREATE_TEMPLATE.format(basedir)

		fileext = path.split('.')[-1]
		
		try:
			db_conn = sqlite3.connect(DB_PATH)
		except:
			logger.error("Could not connect to the database file '{}'".format(DB_PATH))
			return

		db_conn.execute(db_create)
		db_conn.execute('PRAGMA journal_mode=wal')

		if "."+fileext in FACES_FILE_TYPES:
			add_from_file(data_file_path,db_conn,db_insert,logger)
		elif fileext in ".zip":
			add_from_archive(data_file_path,db_conn,db_insert,logger)
		db_conn.close()

	def on_deleted(self, event):

		logger = logging.getLogger('facerec_db_watchdog')
		path = event.src_path
		basedir = path.replace(FACEREC_DATA+os.sep,"").split(os.sep)[0]
		if basedir not in get_facerec_tables():
			return

		db_delete = DB_DELETE_TEMPLATE.format(basedir)

		fileext = path.split('.')[-1]
		logger.debug("Event: file deleted: {}".format(path))
		try:
			db_conn = sqlite3.connect(DB_PATH)
		except:
			logger.error("Could not connect to the database file '{}'".format(DB_PATH))
			return

		is_file = "."+fileext in FACES_FILE_TYPES
		is_archive = (fileext == "zip")
		is_table = len(path.split(os.sep))==2 and (os.path.basename(path) in get_facerec_tables())
		path = path.replace(FACEREC_DATA+os.sep,"")
		if is_file or is_archive or is_table :
			delete_like(db_conn,db_delete,path,logger)
		db_conn.close()

def logger_setup():
	logger = logging.getLogger('facerec_db_watchdog')
	logger.setLevel(logging.INFO)
	trfh = logging.handlers.TimedRotatingFileHandler(
		os.path.join(LOG_PATH,WATCHDOG_LOG_NAME),
		when='W0',
		interval=1,
		backupCount=52)
	formatter = logging.Formatter('[%(asctime)s] - %(levelname)s - %(message)s', datefmt = '%Y-%m-%d %H:%M:%S')
	trfh.setFormatter(formatter)
	logger.addHandler(trfh)
	return logger

def observer_setup():
	observer = PollingObserver()
	observer.daemon = True
	event_handler = FacerecFilesHandler()
	observer.schedule(event_handler, path, recursive=True)
	return observer

if __name__ == "__main__":
	path = sys.argv[1] if len(sys.argv) > 1 else FACEREC_DATA
	logger = logger_setup()
	logger.info("--- Watchdog start ---")
	# Default observer implementation on some OSes misses multiple events, so use PollingObserver
	observer = observer_setup()
	observer.start()
	try:
		while True:
			time.sleep(1)
			if not observer.isAlive():
				logger.warning("Observer stopped running")
				observer.stop()
				observer.join()
				logger.info("Restarting observer...")
				observer.start()
	except KeyboardInterrupt:
		logger.info("Caught KeyboardInterrupt: Stopping observer...")
		observer.stop()
		logger.info("--- Watchdog stop ---")
	observer.join()