#!/bin/bash
mkdir -p data/{facerec,logs,content_detection_models} &&
cp -n tables_list.txt data/facerec/ && rm tables_list.txt;
/webservice/facerec/facerec_db_watchdog.py &
service nginx start &&
uwsgi --ini uwsgi_config.ini --enable-threads